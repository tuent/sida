
[__User__](#user)

### Thông tin API
- Tất cả các API trả về dưới dạng JSON data
- Nếu param nào null thì trả về trống

### User

####  Login 

1. URL: `http://117.6.135.60/api/login`
2. Method: `POST`
3. Params:
    - `username`: string
    - `password`: string
4. Response:
	- `error_code` - int, mã lỗi xảy ra 0 - thành công, còn lại là lỗi
	- `message` - string, message tương ứng: `Đăng nhập thành công`, ...
	- `access_token` - string, token dùng để login
	- `data`
		- `username`
		- `fullname`
		- `email`
		- `edu` - học vấn
		- `dan_toc` - 
		- `nghe_nghiep` - Nghề nghiệp
		- `chieu_cao` - Chiều cao
		- `can_nang` - Cân nặng
 
####  Register
1. URL: `http://117.6.135.60/api/register`
2. Method: `POST`
3. Params:
    - `username`: string
    - `password`: string
    - `email`: string
4. Response:
	- `error_code` - int, mã lỗi xảy ra 0 - thành công, còn lại là lỗi
	- `message` - string, message tương ứng: `Đăng ký thành công`, ...
	- `access_token` - string, token dùng để login

    
#### Get User Info
1. URL: `http://117.6.135.60/api/profile`
2. Method: `GET`
3. Params:
    - `access_token`: Token của user
4. Response:
	- `username`
	- `fullname`
	- `email`
	- `edu` - học vấn
	- `dan_toc` - 
	- `nghe_nghiep` - Nghề nghiệp
	- `chieu_cao` - Chiều cao
	- `can_nang` - Cân nặng
